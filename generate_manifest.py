import json

TOOLBAR = "rgb(41, 47, 52   )"
TOOLBAR_TEXT = "rgb(170, 170, 170)"
TABLINE = "rgb(57, 86, 57)"
ACCENT = "rgb(61, 190, 165)"

base = {
  "manifest_version": 2,
  "version": "1.3",
  "name": "Minimalist Manjaro",
  "theme": {
    "images": {},
    "properties": {},
    "colors": {
      "toolbar": TOOLBAR,
      "toolbar_text": TOOLBAR_TEXT,
      "frame": TOOLBAR,
      "tab_background_text": TOOLBAR_TEXT,
      "toolbar_field": TOOLBAR,
      "toolbar_field_text": TOOLBAR_TEXT,
      "tab_line": TABLINE,
      "popup": TOOLBAR,
      "popup_text": TOOLBAR_TEXT,
      "popup_highlight_text": TOOLBAR,
      "icons_attention": ACCENT,
      "ntp_background": TOOLBAR,
      "ntp_text": TOOLBAR_TEXT,
      "popup_highlight": ACCENT,
      "sidebar_border": TOOLBAR,
      "sidebar_highlight_text": ACCENT,
      "sidebar_text": TOOLBAR_TEXT,
      "sidebar": TOOLBAR,
      "tab_loading": TOOLBAR,
      "tab_selected": ACCENT,
      "tab_text": TOOLBAR,
      "toolbar_bottom_separator": TOOLBAR,
      "toolbar_field_border_focus": ACCENT,
      "toolbar_field_border": TOOLBAR,
      "toolbar_field_focus": TOOLBAR,
      "toolbar_field_highlight": ACCENT,
      "toolbar_field_separator": TOOLBAR,
      "toolbar_field_text_focus": TOOLBAR_TEXT,
      "toolbar_top_separator": TOOLBAR,
      "toolbar_vertical_separator": TOOLBAR
    }
  }
}

print(json.dumps(base, indent=2))
