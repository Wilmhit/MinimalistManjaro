# MinimalistFox

Borderless, minimalist Firefox themes with single coloured active tabs and highlights. Supports version 89+

![Screenshot](screenshot-main.png)

<br/>

## List of themes

### Minimalist Dark and Green Tabs

![Green-tabs](dark-and-green-tabs/green-tabs.svg)

```Colours in use: Black: #282828, Grey: #aaaaaa, Green: #689d69```

Green active tabs and highlights. Inspired by [gruvbox](https://github.com/morhetz/gruvbox). Available on the [Firefox Add-ons site](https://addons.mozilla.org/en-US/firefox/addon/minimalist-dark-and-green-tabs/).

<br/>

### Minimalist Dark and Red Tabs

![Red-tabs](dark-and-red-tabs/red-tabs.svg)

```Colours in use: Black: #1f1f1f, Grey: #8a8a8a, Red: #9d353a```

Red active tabs and highlights. Available on the [Firefox Add-ons site](https://addons.mozilla.org/en-US/firefox/addon/minimalist-dark-and-red-tabs/).

<br/>

### Minimalist Dark and Blue Tabs

![Blue-tabs](dark-and-blue-tabs/blue-tabs.svg)

```Colours in use: Black: #2b303c, Light Grey: #c5c5c7, Blue: #81a1c1```

Blue active tabs and highlights. Inspired by [Nord colour palette](https://github.com/arcticicestudio/nord). Available on the [Firefox Add-ons site](https://addons.mozilla.org/en-US/firefox/addon/minimalist-dark-and-blue-tabs/).

<br/>

### Minimalist Dark and Purple Tabs

![Purple-tabs](dark-and-purple-tabs/purple-tabs.svg)

```Colours in use: Black: #1e1f29, Grey: #aaaaaa, Purple: #725e97```

Purple active tabs and highlights. Inspired by [Dracula-GTK](https://github.com/dracula/gtk). Available on the [Firefox Add-ons site](https://addons.mozilla.org/en-US/firefox/addon/minimalist-dark-and-purple-tab/).

<br/>

### Minimalist Dark and Yellow Tabs

![Yellow-tabs](dark-and-yellow-tabs/yellow-tabs.svg)

```Colours in use: Black: #181b1e, Shadow Green: #9fbfb7, Yellow: #c1af58, Coral Pink: #c1585a```

Yellow active tabs and pink highlights. Inspired by [Sweet Mars theme](https://github.com/EliverLara/Sweet). Available on the [Firefox Add-ons site](https://addons.mozilla.org/en-US/firefox/addon/minimalist-dark-and-yellow-tab/).

